#ifndef LinkedList_H
#define LinkedList_H
#include <cstddef>
#include <iostream>

template<typename T>
struct Node {
    T x;
    Node *next, *prev;
};

template<class T>
class LinkedList {
private:
    Node<T> *head, *tail;
public:
    LinkedList() : head(NULL), tail(NULL) {};
    LinkedList(const LinkedList<T> &other);
    LinkedList(std::initializer_list<T> list) : head{NULL}, tail(NULL) {
        for (auto item : list) {
            add(item);
        }
    }
    ~LinkedList();
    void show();
    void add(T x);
    void addInHead(T x);       
    void insert(T x, int pos);
    void deleteElement(int pos);
    void clear();
    T getFirstElement();
    T getLastElement();
    T getElement(int pos);
    int size();

    LinkedList<T>& operator = (const LinkedList<T> &other) {
        clear();

        Node<T> *temp1 = other.head;
        Node<T> *temp2 = new Node<T>;

        temp2->x = temp1->x;
        temp2->next = NULL;
        temp2->prev = NULL;

        this->head = this->tail = temp2;
        temp1 = temp1->next;
    
        while (temp1 != NULL) {
            this->add(temp1->x);

            temp1 = temp1->next;
        }

        return *this;
    }

    LinkedList<T> operator + (const LinkedList<T> &other) {
        LinkedList<T> list;

        Node<T> *temp = this->head;

        while (temp != NULL) {
            list.add(temp->x);

            temp = temp->next;
        }

        temp = other.head;

        while (temp != NULL) {
            list.add(temp->x);

            temp = temp->next;
        }

        return list;
    }

};

#endif