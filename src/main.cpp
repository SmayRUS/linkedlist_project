#include <iostream>
#include "LinkedList.hpp"

int main () {

    //=================================================
    //
    //      Работа с двухсвязным списком типа int
    //
    //=================================================

    std::cout << "\n=================================================\n" 
        << "Работа с двухсвязным списком типа - int\n" << std::endl;
    LinkedList<int> listInt1;

    std::cout << "Добавление элементов в список\t\t\t";
    listInt1.add(1);
    listInt1.add(2);
    listInt1.add(3);

    listInt1.show();

    std::cout << "Добавление цифры 4 в начало списка\t\t";

    listInt1.addInHead(4);
    listInt1.show();

    std::cout << "Добавление цифры 5 по индексу 1\t\t\t";
    listInt1.insert(5, 1);
    listInt1.show();

    std::cout << "Добавление цифры 6 по индексу 0\t\t\t";
    listInt1.insert(6, 0);
    listInt1.show();

    std::cout << "Добавление цифры 7 по индексу 5\t\t\t";
    listInt1.insert(7, 5);
    listInt1.show();

    std::cout << "Добавление цифры 8 по индексу 7\t\t\t";
    listInt1.insert(8, 7);
    listInt1.show();

    std::cout << "---------------------------------" << std::endl;

    std::cout << "Удаление по индексу 1\t\t\t\t";
    listInt1.deleteElement(1);
    listInt1.show();

    std::cout << "Удаление по индексу 0\t\t\t\t";
    listInt1.deleteElement(0);
    listInt1.show();

    std::cout << "Удаление по индексу 4\t\t\t\t";
    listInt1.deleteElement(5);
    listInt1.show();

    std::cout << "---------------------------------" << std::endl;

    std::cout << "Очистка списка..." << std::endl;
    listInt1.clear();

    std::cout << "Добавление элементов в список\t\t\t";
    listInt1.add(1);
    listInt1.add(2);
    listInt1.add(3);
    listInt1.show();

    std::cout << "Первый элемент списка - " << listInt1.getFirstElement() << std::endl;
    std::cout << "Последний элемент списка - " << listInt1.getLastElement() << std::endl;

    std::cout << "0 элемент списка - " << listInt1.getElement(0) << std::endl;
    std::cout << "1 элемент списка - " << listInt1.getElement(1) << std::endl;
    std::cout << "2 элемент списка - " << listInt1.getElement(2) << std::endl;

    std::cout << "Размер списка - " << listInt1.size() << std::endl;

    std::cout << "---------------------------------" << std::endl;
    
    std::cout << "Новый список с помощью скобочной инициализации\t";
    LinkedList<int> listInt2 {4, 5, 6};

    listInt2.show();

    std::cout << "Присвоение и конкатенация списка listInt3\t";
    LinkedList<int> listInt3 = listInt1 + listInt2;

    listInt3.show();

    //=================================================
    //
    //      Работа с двухсвязным списком типа char
    //
    //=================================================

    std::cout << "\n=================================================\n"
        << "=================================================\n"
        << "Работа с двухсвязным списком типа - char\n" << std::endl;

    LinkedList<char> listChar1;

    listChar1.add('a');
    listChar1.add('b');
    listChar1.add('c');

    std::cout << "Добавление элементов в список\t\t\t";

    listChar1.show();

    std::cout << "Добавление буквы d в начало списка\t\t";

    listChar1.addInHead('d');
    listChar1.show();

    std::cout << "Добавление буквы e по индексу 1\t\t\t";
    listChar1.insert('e', 1);
    listChar1.show();

    std::cout << "Добавление буквы f по индексу 0\t\t\t";
    listChar1.insert('f', 0);
    listChar1.show();

    std::cout << "Добавление буквы g по индексу 5\t\t\t";
    listChar1.insert('g', 5);
    listChar1.show();

    std::cout << "Добавление буквы h по индексу 7\t\t\t";
    listChar1.insert('h', 7);
    listChar1.show();

    std::cout << "---------------------------------" << std::endl;

    std::cout << "Удаление по индексу 1\t\t\t\t";
    listChar1.deleteElement(1);
    listChar1.show();

    std::cout << "Удаление по индексу 0\t\t\t\t";
    listChar1.deleteElement(0);
    listChar1.show();

    std::cout << "Удаление по индексу 4\t\t\t\t";
    listChar1.deleteElement(5);
    listChar1.show();

    std::cout << "---------------------------------" << std::endl;

    std::cout << "Очистка списка..." << std::endl;
    listChar1.clear();

    std::cout << "Добавление элементов в список\t\t\t";
    listChar1.add('a');
    listChar1.add('b');
    listChar1.add('c');

    listChar1.show();

    std::cout << "Первый элемент списка - " << listInt1.getFirstElement() << std::endl;
    std::cout << "Последний элемент списка - " << listInt1.getLastElement() << std::endl;

    std::cout << "0 элемент списка - " << listInt1.getElement(0) << std::endl;
    std::cout << "1 элемент списка - " << listInt1.getElement(1) << std::endl;
    std::cout << "2 элемент списка - " << listInt1.getElement(2) << std::endl;

    std::cout << "Размер списка - " << listInt1.size() << std::endl;

    std::cout << "---------------------------------" << std::endl;
    
    std::cout << "Новый список с помощью скобочной инициализации\t";
    LinkedList<char> listChar2 {'e', 'f', 'g'};

    listChar2.show();

    std::cout << "Присвоение и конкатенация списка listChar3\t";
    LinkedList<char> listChar3 = listChar1 + listChar2;

    listChar3.show();

    std::cout << "=================================================\n" << std::endl;

    return 0;
}