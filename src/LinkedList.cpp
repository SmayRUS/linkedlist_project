#include "LinkedList.hpp"

template <class T>
LinkedList<T>::LinkedList(const LinkedList<T> &other) {
    Node<T> *temp1 = other.head;
    Node<T> *temp2 = new Node<T>;

    temp2->x = temp1->x;
    temp2->next = NULL;
    temp2->prev = NULL;

    this->head = this->tail = temp2;
    temp1 = temp1->next;
 
    while (temp1 != NULL) {
        this->add(temp1->x);

        temp1 = temp1->next;
    }
}

template <class T>
LinkedList<T>::~LinkedList() {
    LinkedList::clear();
}

//====================================
//
//          Public Methods
//
//====================================

template <class T>
void LinkedList<T>::show() { 
    Node<T> *temp = head;

    while (temp != NULL)
    {
        std::cout << temp->x << " ";
        temp = temp->next;
    }

    std::cout << std::endl;
}

template <class T>
void LinkedList<T>::add(T x) {
    Node<T> *temp = new Node<T>;
    temp->next = NULL;
    temp->x = x;
 
    if (head != NULL) {
        temp->prev = tail; 
        tail->next = temp;
        tail = temp;
    } else {
        temp->prev = NULL;
        head = tail = temp;
    }
}

template <class T>
void LinkedList<T>::addInHead(T x) {
    Node<T> *temp = new Node<T>;

    temp->x = x;
    temp->next = head;

    head->prev = temp;
    head = temp;
}

template <class T>
void LinkedList<T>::insert(T x, int pos) {
    Node<T> *temp1 = head,
        *temp2 = new Node<T>();

    int listLen = LinkedList<T>::size();

    if (listLen < pos) {
        throw std::out_of_range("LinkedList<T>::insert() - Index out of range.");
    }

    if (pos == 0) {
        LinkedList::addInHead(x);
        return;
    }

    temp2->x = x;

    for (int i = 0; i < pos-1; i++) {
        temp1 = temp1->next;
    }

    if (listLen != pos) {
        temp2->next = temp1->next;
        temp1->next = temp2;
    } else {
        temp2->prev = temp1;
        temp1->next = temp2;
        tail = temp2;
    }
}

template <class T>
void LinkedList<T>::deleteElement(int pos) {
    if (head == NULL) {
        std::cout << "LinkedList<T>::deleteElement() - List empty." << std::endl;
        return;
    }

    Node<T> *temp1 = head, *temp2 = NULL;
    
    int listLen = LinkedList<T>::size();

    if (listLen <= pos) {
        throw std::out_of_range("LinkedList<T>::deleteElement() - Index out of range.");
    }

    if (pos == 0) {
        head = head->next;
        delete temp1;
        return;
    }

    for (int i = 0; i < pos; i++) {
        temp2 = temp1;
        temp1 = temp1->next;
    }

    if (listLen-1 != pos) {
        temp2->next = temp1->next;
        delete temp1;
    } else {
        temp2 = temp1;
        tail = temp2->prev;
        tail->next = NULL;
        delete temp2;
    }

    
}

template <class T>
void LinkedList<T>::clear() {
    while (head)
    {
        tail = head->next;
        delete head;
        head = tail;
    }
}

template <class T>
T LinkedList<T>::getFirstElement() {
    return head->x;
}

template <class T>
T LinkedList<T>::getLastElement() {
    return tail->x;
}

template <class T>
T LinkedList<T>::getElement(int pos) {
    Node<T> *temp = head;

    int listLen = LinkedList<T>::size();

    if (listLen <= pos) {
        throw std::out_of_range("LinkedList<T>::getElement() - Index out of range.");
    }

    while (pos-- > 0) {
        temp = temp->next;
    }

    return temp->x;
}

template <class T>
int LinkedList<T>::size() {
    Node<T> *temp = head;
    int listLen = 0;

    while (temp != NULL) {
        temp = temp->next;
        listLen++;
    }

    return listLen;
}

template class LinkedList<int>;
template class LinkedList<long>;
template class LinkedList<double>;
template class LinkedList<float>;
template class LinkedList<char>;